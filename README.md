Bool Cow game. powered by Spring, PostgreSQL.

### Game Description ###

The computer thinks of a 4 digit number. Numbers of the hidden number are not
repeat. The task of the user to guess the number. At
user unlimited number of attempts. In every attempt
the user gives the computer its own variant of the number. The computer reports
how many digits are accurately predicted (bull) and how many digits are not considered
positions (cow). When the computer responds, the user must
a few of the moves to guess the number.

Example:
   7328 -- a mysterious number
   0819 -- 0B1C
   4073 -- 0B2C
   5820 -- 1B1C
   3429 -- 1B1C
   5960 -- 0B0C
   7238 -- 2B2C
   7328 -- 4Б0C (number of correctly predicted)
   
Do not forget create DB and Artefact