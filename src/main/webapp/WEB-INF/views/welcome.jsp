<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html; charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome in BullCow Game</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/JavaScript"
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js">
    </script>


    <script type="text/javascript">

        var countInput=0;

        function getResult(i) {

            if (countInput <= 4) {
                var o = document.getElementById('button_' + i);

                o.disabled = true;
                document.getElementById("userInputText").innerText += i;
                countInput++;
            }
            if (countInput==4)
            {
                document.location.href = "${contextPath}/welcome?inputString="+document.getElementById("userInputText").innerText;
                countInput=0;
            }
        }
    </script>

</head>
<body>

<div class="container">

    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome in Bull Cow game, ${pageContext.request.userPrincipal.name} | <a href="${contextPath}/rating">Rating</a> | <a onclick="document.forms['logoutForm'].submit()">Logout</a>
        </h2>


        <form id = "inputForm">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <div id="digitGrid" align="center" style="padding-right: 250px;">
            <p id = "userInputText"></p>
            <p id = "stepText"></p>
            <button id = button_0 onclick="getResult(0)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">0</button>
            <button id = button_1 onclick="getResult(1)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">1</button>
            <button id = button_2 onclick="getResult(2)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">2</button>
            <button id = button_3 onclick="getResult(3)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">3</button>
            <button id = button_4 onclick="getResult(4)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">4</button>
            <button id = button_5 onclick="getResult(5)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">5</button>
            <button id = button_6 onclick="getResult(6)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">6</button>
            <button id = button_7 onclick="getResult(7)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">7</button>
            <button id = button_8 onclick="getResult(8)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">8</button>
            <button id = button_9 onclick="getResult(9)" style = "width: auto; display: inline; margin-top: 0px" class="btn btn-lg btn-primary btn-block" type="submit">9</button>


            <table id = "tab1"; class="table table-sm">
                <thead>
                <tr>
                    <th style="text-align: center" scope="col">Step list</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${stepList}" var="step">
                    <tr>
                        <td style="text-align: center">${step}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        </form>

    </c:if>

</div>

<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>