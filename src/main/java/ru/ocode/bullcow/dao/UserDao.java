package ru.ocode.bullcow.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.ocode.bullcow.model.User;

import java.util.List;

/**
 * Data Access Object for {@link User}.
 *
 * @version 1.0
 */
public interface UserDao extends JpaRepository<User, Long>
{
    /**
     * return {@link User} by username.
     *
     * @param username name of {@link User}.
     */
    User findByUsername(String username);

    /**
     * return {@link List} of {@link User} for rating. Best 10 players.
     */
    List<User> findTop10ByOrderByRating();//findAllByOrderByRating()

    /**
     * update {@link User} rating by username and last game rating.
     *
     * @param username name of {@link User}
     * @param lastGameRating rating in last game
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE bullcow.users SET rating = (rating+?1)/2 WHERE bullcow.users.username = ?2", nativeQuery = true)
    void updateRating(int lastGameRating,String username);
}
