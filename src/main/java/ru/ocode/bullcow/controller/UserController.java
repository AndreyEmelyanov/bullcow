package ru.ocode.bullcow.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ocode.bullcow.model.BullCow;
import ru.ocode.bullcow.model.User;
import ru.ocode.bullcow.service.SecurityService;
import ru.ocode.bullcow.service.UserService;
import ru.ocode.bullcow.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller class in Bull Cow game
 *
 * @version 1.0
 */
@Controller
public class UserController
{

    /**
     * Interface for store {@link User}
     */
    @Autowired
    private UserService userService;

    /**
     * Interface for Security
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Class for Validation of {@link User}
     */
    @Autowired
    private UserValidator userValidator;

    /**
     * for control new hidden
     */
    private boolean hiddenFlag = true;

    /**
     * hidden number
     */
    private String hidden;

    /**
     * the list of attempts of guessing
     */
    private List<String> stepList = new ArrayList<>();

    /**
     * Maping registrarion.jsp
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model)
    {
        stepList.clear();
        model.addAttribute("userForm", new User());

        return "registration";
    }

    /**
     * validate data in registrarion.jsp
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model)
    {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors())
        {
            return "registration";
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getConfirmPassword());

        return "redirect:/welcome";
    }

    /**
     * Mapping login.jsp
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout)
    {
        stepList.clear();
        if (error != null)
        {
            model.addAttribute("error", "Username or password is incorrect.");
        }

        if (logout != null)
        {
            model.addAttribute("message", "Logged out successfully.");
        }
        return "login";
    }

    /**
     * Mapping welcome.jsp. Main game.
     */
    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public ModelAndView welcome(Model model,@RequestParam(value = "inputString", required = false)   String inputString)
    {
        ModelAndView mov = new ModelAndView();
        if (inputString!=null && !inputString.equals(""))
        {
            if (hiddenFlag)
            {
                hidden = BullCow.getHidden();             //can see hidden
                stepList.add(0,inputString +" - "+ BullCow.getBullCowByInputAndHidden(inputString.toString(), hidden));// reverse
                hiddenFlag = false;
            }
            else
            {
                stepList.add(0,inputString +" - "+ BullCow.getBullCowByInputAndHidden(inputString.toString(), hidden));
            }
        }
        if (stepList.size() !=0 && (stepList.get(0).charAt(7)=='4'))
        {
            mov.addObject("stepCount",stepList.size());
            Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();

            mov.addObject("user",loggedInUser.getName());
            mov.setViewName("congratulation");

            userService.updateRating(stepList.size(),loggedInUser.getName());
            stepList.clear();
            hiddenFlag = true;
        }
        else
        {
            mov.addObject("stepList", stepList);
            mov.setViewName("welcome");
        }
        return mov;
    }

    /**
     * Mapping rating.jsp
     */
    @RequestMapping(value = {"/rating"}, method = RequestMethod.GET)
    public ModelAndView users()
    {
        ModelAndView mov = new ModelAndView();
        List<User> list = userService.findTop10ByOrderByRating();
        mov.addObject("userList", list);
        mov.setViewName("rating");
        return mov;
    }
}
