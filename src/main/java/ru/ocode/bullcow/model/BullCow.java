package ru.ocode.bullcow.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Object that contains Bool Cow game logic.
 *
 * @version 1.0
 */
public class BullCow
{
    /**
     * Length hidden number.
     */
    private static final int HIDDEN_LENGTH = 4;

    /**
     * List randomize for hidden.
     */
    private static List<String> helpHidden = Arrays.asList("0","1","2","3","4","5","6","7","8","9");

    /**
     * Return result of guessing.
     *
     * @param userInput users guessing string.
     * @param hidden number hidden by the computer.
     */
    public static String getBullCowByInputAndHidden(String userInput, String hidden)
    {
        HashMap<Integer, Character> map = new HashMap<>();

        for (int i = 0; i < userInput.length(); i++)
        {
            map.put(i, userInput.charAt(i));
        }

        int bull = 0;
        int cow = 0;
        for (int i = 0; i < hidden.length(); i++)
        {
            if (map.containsValue(hidden.charAt(i)))
            {
                if (userInput.charAt(i) == hidden.charAt(i))
                {
                    bull++;
                }
                else
                {
                    cow++;
                }
            }

        }
        return (bull + "B" + cow + "C");
    }

    /**
     * return random number hidden by the computer.
     */
    public static String getHidden()
    {
        Collections.shuffle(helpHidden);
        return  String.join("", helpHidden.subList(0, HIDDEN_LENGTH));
    }
}
