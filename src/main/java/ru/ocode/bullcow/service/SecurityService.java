package ru.ocode.bullcow.service;

/**
 * Interface for Security
 *
 * @version 1.0
 */
public interface SecurityService
{
    /**
     * Return Name of current {@link ru.ocode.bullcow.model.User}
     */
    String findLoggedInUsername();

    /**
     * Auto login.
     *
     * @param username name of {@link ru.ocode.bullcow.model.User}
     * @param password password of {@link ru.ocode.bullcow.model.User}
     */
    void autoLogin(String username, String password);
}
