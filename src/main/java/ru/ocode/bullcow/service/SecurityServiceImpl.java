package ru.ocode.bullcow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link SecurityService}.
 *
 * @version 1.0
 */
@Service
public class SecurityServiceImpl implements SecurityService
{

    /**
     * for authenticate User.
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * for find information about User.
     */
    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * Return Name of current {@link ru.ocode.bullcow.model.User}
     */
    @Override
    public String findLoggedInUsername()
    {
        Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (userDetails instanceof UserDetails)
        {
            return ((UserDetails) userDetails).getUsername();
        }

        return null;
    }

    /**
     * Auto login.
     *
     * @param username name of {@link ru.ocode.bullcow.model.User}
     * @param password password of {@link ru.ocode.bullcow.model.User}
     */
    @Override
    public void autoLogin(String username, String password)
    {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

        authenticationManager.authenticate(authenticationToken);

        if (authenticationToken.isAuthenticated())
        {
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
    }
}