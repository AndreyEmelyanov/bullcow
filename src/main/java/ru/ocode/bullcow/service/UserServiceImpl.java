package ru.ocode.bullcow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ocode.bullcow.dao.RoleDao;
import ru.ocode.bullcow.dao.UserDao;
import ru.ocode.bullcow.model.Role;
import ru.ocode.bullcow.model.User;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of {@link UserService}.
 *
 * @version 1.0
 */
@Service
public class UserServiceImpl implements UserService
{

    /**
     * Data Access Object for {@link User}.
     */
    @Autowired
    private UserDao userDao;

    /**
     * Data Access Object for {@link Role}.
     */
    @Autowired
    private RoleDao roleDao;

    /**
     * Password coder in Spring Security
     */
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * save {@link User} in DB.
     */
    @Override
    public void save(User user)
    {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.getOne(1L));
        user.setRoles(roles);
        userDao.save(user);
    }

    /**
     * return {@link User} by username.
     *
     * @param username name of {@link User}.
     */
    @Override
    public User findByUsername(String username)
    {
        return userDao.findByUsername(username);
    }

    /**
     * return {@link List} of {@link User} for rating. Best 10 players.
     */
    @Override
    public List<User> findTop10ByOrderByRating()
    {
        return userDao.findTop10ByOrderByRating();
    }

    /**
     * update {@link User} rating by username and last game rating.
     *
     * @param username name of {@link User}
     * @param lastGameRating rating in last game
     */
    @Override
    public void updateRating(int lastGameRating, String username)
    {
        userDao.updateRating(lastGameRating,username);
    }
}
