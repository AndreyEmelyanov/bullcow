CREATE DATABASE BullCow;

CREATE SCHEMA BullCow;

CREATE TABLE bullcow.users (
  id serial PRIMARY KEY,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  rating INT
);


CREATE TABLE bullcow.roles (
  id  serial PRIMARY KEY,
  name VARCHAR(100) NOT NULL
);

INSERT INTO bullcow.roles VALUES (1, 'ROLE_USER');


CREATE TABLE bullcow.user_roles (
  user_id INT NOT NULL,
  role_id INT NOT NULL,

  FOREIGN KEY (user_id) REFERENCES bullcow.users (id),
  FOREIGN KEY (role_id) REFERENCES bullcow.roles (id),

  UNIQUE (user_id, role_id)
);

